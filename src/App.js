import React from 'react';
import {BrowserRouter, Switch, Route}from 'react-router-dom'
import * as Views from './reducers'
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Views.Index}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
