import React,{ useState, useEffect } from 'react'
import Login from '../components/functionals/Login'
import { useDispatch } from 'react-redux'

const styles ={
    color:'white',
    
}

const Index = props =>{
    const [ login, setLogin ]= useState({
        mail:'',
        password:''
    })
    const handleLogin = values =>{
        setLogin({
            ...login,
            [login]:values
        })
    }
    
    return(
        <div style={styles}>
            <Login onSubmit={handleLogin}/>
        </div>
    )
}