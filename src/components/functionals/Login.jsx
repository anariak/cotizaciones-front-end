import React from 'react'
import { Field, reduxForm } from 'redux-form'

const LoginForm = props =>{
    const { handleSubmit } = props
    return(
        <form onSubmit={handleSubmit}>
            <label htmlFor="correo"> mail:
                <Field  name="correo" type="text" component="input" />
            </label>
            <label htmlFor="password"> contraseña:
                <Field name="password" type="password" component="input" />
            </label>
            <input type="submit">Ingresar</input>
        </form>
    )
}

export default reduxForm({
    form:'LoginForm'
})(LoginForm)