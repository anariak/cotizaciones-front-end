import React from 'react'



const PersonalTextBox = props =>{
    const { name, type, required} = props
    return(
        <input style={{
            borderStyle:'solid',
            borderColor:"gray",
            borderTop:"none",
            borderLeft:"none",
            MozBoxShadow:"1"
        }}
        name={name}
        type={type}
        required={required}
        />
    )
}

export default PersonalTextBox;