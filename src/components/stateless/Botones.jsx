import React from 'react'

const Botones = props => {
    const { name, value, direccion} = props
    return(
        <button style={{
            backgroundColor:"#2c73b8",
            maxWidth:"auto",
            height:"auto",
            fontFamily:"'Open Sans', sans-serif;"
        }}  
            name={name}
            value={value}
            onClick={direccion}
        >{name}</button>
    )
}

export default Botones