const ADD = 'USUARIO/ADD'
const UPDATE ='USUARIO/UPDATE'
const READ = 'USUARIO/READ'

const add_usuario = payload =>({
    type: ADD,
    payload
})

const update_usuario = payload =>({
    type: UPDATE,
    payload
})

const read_usuario = payload =>({
    type: READ,
    payload
})

