const ADD = 'PRODUCTOS/ADD'
const UPDATE = 'PRODUCTOS/UPDATE'
const READ = 'PRODUCTOS/READ'

const add_producto = payload =>({
    type: ADD,
    payload
})
const update_producto = payload =>({
    type: UPDATE,
    payload
})
const read_producto = payload =>({
    type: READ,
    payload
})

const initialState = {}
const producto =(state = initialState, action)=>{
    switch (action.type) {
        case ADD:
            return{
                ...state,
                producto:[state.producto, action.type]
            }
        case UPDATE:
            return{
                ...state,

            }
        case READ:
        default:
            break;
    }
}

export default producto