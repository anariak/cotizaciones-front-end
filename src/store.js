import { createStore, combineReducers, applyMiddleware } from 'redux'
import { reducer as formReducer } from 'redux-form'
import * as reducer from './reducers'
import thunk from 'redux-thunk'

const rootReducer = combineReducers({
    reducer,
    form: formReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk))
export default store